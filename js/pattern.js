var patternsData
class Pattern {
  RegisterOnReady () {
    $($.proxy(this.onReady, this))
  }
  onReady () {
    console.log('Pattern.onReady')
    var it = this
    $.ajax({
      type: 'GET',
      url: 'https://api.myjson.com/bins/crrrn',
      success: function (data) {
        patternsData = data.patterns
        var html = Pattern.GetSelect(data.patterns, '0', true)
        $('#Pattern').append(html)
        Pattern.FillTable(patternsData['0'])
        $('.condition').show()
      },
      error: function (jqXHR, textStatus, errorThrown) {
        alert(jqXHR.status)
      },
      dataType: 'json'
    })
  }
  get selectedPattern () {
    return parseInt($('#Pattern').children('option:selected').val())
  }
  get patternsData () {
    return patternsData
  }
  static FillTable (json) {
    var html = ''
    var i = 0
    json.steps.forEach((result) => {
      html += Pattern.GetHtmlRow(result)
      i += 1
      if (i === json.steps.length) {
        $('tbody').html(html)
      }
    })
  }
  static GetSelect (json, selected, isAjax) {
    var html = ''
    for (var property in json) {
      html += '<option value="' + property + '"'
      if (selected === property) {
        html += ' selected="selected"'
      }
      if (isAjax === true) {
        html += '>' + json[property].name + '</option>'
      } else {
        html += '>' + json[property] + '</option>'
      }
    }
    return html
  }
  static GetHtmlRow (step) {
    let settings = $.extend({
      if: '#FFFFFF',
      then: {
        color: '#FFFFFF',
        direction: 'left'
      }
    }, step)

    let html = '<tr data-if-color="' + settings.if + '">'
    html += '<td class="if-color">' + PatternColor[settings.if] + '</td>'
    html += '<td class="then-color"><select>' + Pattern.GetSelect(PatternColor, settings.then.color, false) + '</select></td>'
    html += '<td class="then-direction"><select>' + Pattern.GetSelect(PatternDirection, settings.then.direction, false) + '</select></td>'
    html += '</tr>'
    return html
  }
}

const PatternColor = Object.freeze({
  '#FFFFFF': 'Blanc',
  '#6D9ECE': 'Bleu Clair',
  '#1F5FA0': 'Bleu Fonc&eacute;',
  '#6D071A': 'Bordeaux',
  '#606060': 'Gris',
  '#F0C300': 'Jaune',
  '#000000': 'Noir',
  '#FF7F00': 'Orange',
  '#E0115F': 'Rose',
  '#DB1702': 'Rouge',
  '#008020': 'Vert',
  '#7F00FF': 'Violet'
})

const PatternDirection = Object.freeze({
  'left': 'Gauche',
  'right': 'Droite'
})
