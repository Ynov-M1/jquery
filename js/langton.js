/// <reference path="ant.js" />
/// <reference path="grid.js" />
/// <reference path="pattern.js" />
/// <reference path="simulation.js" />
var tid
var ancientColor
class Langton {
  constructor () {
    this.Pattern = new Pattern()
    this.Simulation = new Simulation()
  }
  RegisterOnReady () {
    this.Pattern.RegisterOnReady()
    this.Simulation.RegisterOnReady()

    $($.proxy(this.onReady, this))
  }
  onReady () {
    $('#Reset').on('click', $.proxy(this.onBtnResetClick, this))
    $('ul').on('click', $.proxy(this.onBtnResetClick, this))
    $('#MoveForward').on('click', $.proxy(this.onBtnMoveClick, this))
    $('#Start').on('click', $.proxy(this.onBtnStartClick, this))
    $('#Pattern').on('change', $.proxy(this.onBtnPatternClick, this))
    $('#CurrentPattern,.then-color').on('change', function (e) {
      this.onPatternChange(e)
    }.bind(this))

    $('#CurrentPattern,.then-color').on('click', function (e) {
      ancientColor = $(e.target).val()
    })

    this.Grid = new Grid('Grid', this.Simulation.Size)
    this.Ant = new Ant(this.Grid.MiddleX, this.Grid.MiddleY)
    this.displayAntInfo()

    $(this.Ant).on('move', $.proxy(this.displayAntInfo, this))

    console.log('Langton.onReady')
  }
  displayAntInfo () {
    this.Grid.SetColor(this.Ant.X, this.Ant.Y, Ant.Color)
    $('.ant-x').text(this.Ant.X)
    $('.ant-y').text(this.Ant.Y)
    $('.ant-direction').text(this.Ant.Direction)
    $('.ant-nb-steps').text(this.Ant.NbSteps)
  }
  onBtnResetClick () {
    clearInterval(tid)
    $('#Start').html('D&eacute;marrer')
    this.Grid = new Grid('Grid', this.Simulation.Size)
    this.Ant = new Ant(this.Grid.MiddleX, this.Grid.MiddleY)
    this.Ant.Reset(this.Grid.MiddleX, this.Grid.MiddleY)
    $(this.Ant).on('move', $.proxy(this.displayAntInfo, this))
    this.displayAntInfo()
  }
  onBtnMoveClick () {
    for (var i = 0; i < this.Simulation.NbEtapes; i++) {
      if (this.Grid.GetColor(this.Ant.X, this.Ant.Y) === '#FFFFFF') {
        this.Grid.SetColor(this.Ant.X, this.Ant.Y, '#000000')
        this.Ant.Turn('right')
      } else {
        this.Grid.SetColor(this.Ant.X, this.Ant.Y, '#FFFFFF')
        this.Ant.Turn('left')
      }
    }
  }
  onBtnStartClick () {
    var pattern = this.Pattern.patternsData[this.Pattern.selectedPattern].steps
    if ($('#Start').text() === 'Stop') {
      $('#Start').html('D&eacute;marrer')
      clearInterval(tid)
    } else {
      $('#Start').html('Stop')
      tid = setInterval(() => {
        for (var i = 0; i < pattern.length; i++) {
          if (this.Grid.GetColor(this.Ant.X, this.Ant.Y) === null) {
            clearInterval(tid)
          } else if (this.Grid.GetColor(this.Ant.X, this.Ant.Y) === pattern[i].if) {
            this.Grid.SetColor(this.Ant.X, this.Ant.Y, pattern[i].then.color)
            this.Ant.Turn(pattern[i].then.direction)
          }
        }
      }, this.Simulation.Interval)
    }
  }
  onBtnPatternClick () {
    Pattern.FillTable(this.Pattern.patternsData[this.Pattern.selectedPattern])
    this.onBtnResetClick()
  }
  addRow (val) {
    var result = { 'if': val, 'then': { 'color': '#FFFFFF', 'direction': 'right' } }
    this.Pattern.patternsData[this.Pattern.selectedPattern].steps.push(result)
    let html = Pattern.GetHtmlRow(result)
    $('tbody tr:last').after(html)
  }
  onPatternChange (e) {
    var pattern = this.Pattern.patternsData[this.Pattern.selectedPattern].steps
    var val = $(e.target).val()
    var needDelete = false
    var selectColor = ''
    var listDel = []
    for (var i = 0; i < pattern.length; i++) {
      if (needDelete) {
        listDel.push(i)
      }
      if (pattern[i].then.color === val) {
        alert('COULEUR ' + val + 'DEJA SELECTIONNEE')
        $(e.target).val(ancientColor)
        return
      } else if (ancientColor === pattern[i].then.color) {
        selectColor = pattern[i].if
        needDelete = true
      }
    }
    for (var i = 0; i < listDel.length; i++) {
      this.Pattern.patternsData[this.Pattern.selectedPattern].steps.pop()
    }
    var len = this.Pattern.patternsData[this.Pattern.selectedPattern].steps.length
    $('tr[data-if-color="' + selectColor + '"]').nextAll().remove()

    if (val !== '#FFFFFF') {
      this.Pattern.patternsData[this.Pattern.selectedPattern].steps[len - 1].then.color = val
      this.addRow(val)
    }
  }
}

let langton = new Langton()
langton.RegisterOnReady()
